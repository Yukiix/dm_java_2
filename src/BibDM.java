import java.util.Collections;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;
public class BibDM{


    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return new Integer(a+b);
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      Integer mini = null;
      for(Integer elem:liste){
          if(mini==null || elem.compareTo(mini)<0){
              mini = elem;
          }
      }
      return mini;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if ( liste.isEmpty() )
          return true;
        return valeur.compareTo(Collections.min(liste))<0;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> res = new ArrayList<>();
      Integer x=0;
      Integer y=0;
      while(x<liste1.size() && y<liste2.size()){
          T a = liste1.get(x);
          T b = liste2.get(y);
          if(a.compareTo(b)==0 && !res.contains(a)){
              res.add(a);
              x+=1;
              y+=1;
          }
          else if(a.compareTo(b)<0){
              x+=1;
          }
          else{
              y+=1;
          }
      }
      return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      List<String> res = new ArrayList<>();
      String[] decoupe = texte.split(" ");
      for(int cpt=0;cpt<decoupe.length;cpt++){
          if(!decoupe[cpt].equals(""))
              res.add(decoupe[cpt]);
      }
      return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if ( texte.isEmpty() )
          return null;

        List<String> liste = BibDM.decoupe(texte);
        Map<String,Integer> DicoMotOccu = new HashMap<>();
        for(String mot:liste){
            if(DicoMotOccu.containsKey(mot))
                DicoMotOccu.put(mot,new Integer(DicoMotOccu.get(mot)+1));
            else
                DicoMotOccu.put(mot, new Integer(1));
        }
        Entry<String, Integer> motMajoritaire = null;
        for (Entry<String, Integer> entry : DicoMotOccu.entrySet()) {

          if (motMajoritaire == null || motMajoritaire.getValue() < entry.getValue())
              motMajoritaire = entry;
          else if (motMajoritaire.getKey().compareTo(entry.getKey())>0){
              motMajoritaire = entry;
            }
        }
        return motMajoritaire.getKey();
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      if(chaine.isEmpty())
          return true;
      int G = 0;
      int D = 0;
      char last = ' ';
      for ( int i=0 ; i<chaine.length() ; i++){
        if ( chaine.charAt(i)=='(' ){
          G++;
          last=chaine.charAt(i);
        }
        else if ( chaine.charAt(i)==')' ){
          if ( G==0 )
            return false;
          D++;
          last=chaine.charAt(i);
        }
      }
      if ( last=='(' )
        return false;
      return G==D;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        char last = ' ';
        int NbPG = 0;
        int NbPD = 0;
        int NbCG = 0;
        int NbCD = 0;
        for ( int i=0; i<chaine.length(); i++){

          if ( last == ' ' )
            last = chaine.charAt(i);

          else if ( last=='(' && chaine.charAt(i)==']' || last=='[' && chaine.charAt(i)==')' )
            return false;

          if ( chaine.charAt(i)=='(' )
            NbPG++;

          else if ( chaine.charAt(i)==')' ){
            if ( NbPG==0 ) // false si il n'y a pas de parenthese ouvrante avant
              return false;
            NbPD++;
          }
          else if ( chaine.charAt(i)=='[' )
            NbCG++;

          else if ( chaine.charAt(i)==']' ){
            if ( NbCG==0 )
              return false;
            NbCD++;
          }
          if ( chaine.charAt(i)=='(' || chaine.charAt(i)==')' || chaine.charAt(i)=='[' ||  chaine.charAt(i)==']' )
            last = chaine.charAt(i);
        }
        if ( last=='(' || last=='[' )
          return false;
        return NbPG == NbPD && NbCG == NbCD;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.isEmpty())
          return false;
        int min = 0;
        int max=liste.size();
        int mid;
        while(min<max){
            mid = ((min+max)/2);
            if(valeur.compareTo(liste.get(mid))==0)
                return true;
            else if(valeur.compareTo(liste.get(mid))>0)
                min = mid+1;
            else
                max = mid;
        }
        return false;
    }



}
